$(function () {
    $(".js-ajax-form").on("submit", function (e) {
        e.preventDefault;
        var form = $(this);
        var output = $("." + form.data("holder"));

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (response) {
            output.html(response);
        });;

        return false;
    });
});
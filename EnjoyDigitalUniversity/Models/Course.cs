﻿using System.Web;

namespace EnjoyDigitalUniversity.Models
{
    public class Course
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public IHtmlString BodyText { get; set; }

        public string Department { get; set; }

        public string Url { get; set; }
    }
}